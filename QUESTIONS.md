# DRAFT
# Questions


## How long have you been working with Racket?
## How long did it take to get productive in Racket?
## How long have you used Racket and still fee unproductive?


## If you summed the size of all Racket projectes you work on, how big would it be?
I don't know | less than 1000 lines | 1000 to 10,000 lines | 10,000 to 100,000 lines | more than 100,000 lines

## How regularly do you work with Racket" 
rarely/daily/weekly/monthly

## Reasons why you chose to learn/develop in racket?

* [ ] 'batteries included'/cross platform
* [ ] LOP / wanted to make own language
* [ ] a 'Lisp' / SICP / learning
* [ ] required by school/college
* [ ] other reasons (comma sep); [...,...,...,...]


## Do you use Racket specifically for it's DSL capabilities?

## My involvement in the Racket ecosystem includes
* [ ] Happily using the language and libs
* [ ] Building services or products using Racket
* [ ] Advocating for Racket in my organization
* [ ] Helping new Racket users
* [ ] Creating/maintaining open source libraries
* [ ] Filing issues on Racket libs
* [ ] Providing PRs or patches on Racket libs
* [ ] Writing about Racket
* [ ] Speaking about Racket at conferences
* [ ] Creating Racket developer tools
* [ ] Providing Racket training
* [ ] Voting on issues for the language itself
* [ ] Running Racket meetups, confs or workshops
* [ ] Filing issues on the language itself
* [ ] Contributing documentation
* [ ] Providing patches for language issues
* [ ] Creating Racket podcasts, screencasts, or videos

## How would you rate the difficulty of learning these concepts/topics?
1. Contracts
2. Iterators and comprehensions
3. pattern matching
4. Class, objects, traits and mixins
5. Units (components)
6. Reflection and Dynamic Evaluation
7. Macros
8. Reader Extensions
9. Defining new #lang Languages
10. Concurrency and Synchronization
11. Parallelism
12. Scribbling documentation
13. Making Packages
14. Compiling and Creating Stand-Alone Executables
15. Typed racket
16. Control flow; exceptions, delayed evaluation, Continuations, Adding Control Operators, Continuation Marks
17. foreign function interface
18. Web server and Servlets
19. The Racket Graphical Interface Toolkit
20. Extending DrRacket with scripts, teachpacks, or plugins
21. Other - please specify

Diffulty scale
-[ ] N/A (haven't looked at this yet)
-[ ] Easy
-[ ] Moderate
-[ ] Tricky
-[ ] Very Difficult
-[ ] Still don't get it 

## What programming languages are you comfortable with?
-[ ] C
-[ ] C++
-[ ] C#
-[ ] Clojure
-[ ] Elm
-[ ] Elixr
-[ ] Erlang
-[ ] Go
-[ ] Haskell
-[ ] Java
-[ ] Javascript
-[ ] Objective-C
-[ ] PHP
-[ ] Python
-[ ] Ruby
-[ ] Rust
-[ ] Scala
-[ ] Swift
-[ ] Typescript
-[ ] Perl
-[ ] Lua
-[ ] Kotlin
-[ ] R
-[ ] CSS
-[ ] other - please specify

## Which version of Racket do you use for your applications?
-[ ] Current stable
-[ ] Previous stable
-[ ] Nightly
-[ ] I don't know

## Has upgrading to the current stable version broken any of your code in the past year?
yes/no

## How much work did it take to fix your code when you upgraded to the current stable version?
(scale of 1 to 5)

## What platforms so you use Racket on 
-[ ] Windows
-[ ] Linux x86 (desktop/server)
-[ ] Linux ARM (Android phone, Rpi,chromebook, embedded)
-[ ] BSD variant
-[ ] MacOS 

## Display size
-[ ] Large and/or multimonitor
-[ ] 15in laptop
-[ ] 13in laptop
-[ ] other

## What is your preferred ways of installing Racket?
official installer from https://download.racket-lang.org
-[ ] Ubuntu PPA 
-[ ] macos Homebrew
-[ ] Build from source

## What Editors do you use when writing Racket?
-[ ] DrRacket
-[ ] Racket Mode (on Emacs)
-[ ] VSCode
-[ ] Vim
-[ ] Other - please specify


## Do you feel welcome in the Racket community
yes/no/I don't know

## Prefered channels for community participation?
-[ ] Racket Users (web)
-[ ] Racket Users (email list)
-[ ] IRC
-[ ] Slack
-[ ] Reddit r/racket
-[ ] Discord
-[ ] Discourse
-[ ] Twitter
-[ ] Stack Overflow
-[ ] IRL (meetups/conference/workshop - please specify)
-[ ] Facebook
-[ ] Other chat - please specify
-[ ] Other forum - please specify

## Do you consider yourself a member of an underrepresented demographic in technology?
I don't know/No/yes but prefer not to specify/yes (please specify)
e.g. BAME, Woman, non-Binary, gay, trans, language spoken, mobility, sight, hearing, age (young, old), any other. 


## What primary industry do you develop for?

## What has been most frustrating or has prevented you from using Racket more than you do now?
-[ ] Convincing coworkers/company/clients
-[ ] Error messages
-[ ] Hiring and staffing
-[ ] Need better tools / IDEs
-[ ] Need better guides or tutorials
-[ ] Need better reference docs
-[ ] Scripting difficulties
-[ ] Finding libraries
-[ ] Long-term viability
-[ ] Runtime performance
-[ ] No static typing
-[ ] App deployment
-[ ] Installation process
-[ ] Portability between dialects
-[ ] Unpleasant community
-[ ] Version incompatibility
-[ ] Release schedule
-[ ] Other - please specify

Rate the priority of making improvements to Clojure in these areas.
Error messages
Reference docs
Spec
Startup time
Tutorials
Runtime performance
Core library
Collections
Memory management
Concurrency
Portability
Java interop
Namespaces
AOT

## Non-users

How long did you use Racket before you stopped?
(time periods)


## Suggestions

Comments on how Racket and its community can improve:
* missing libraries?
* community facilities?
* ???


# TODO: Go survey review; 


```

Questions from the Golang survey 2018
https://blog.golang.org/survey2018-results
Programming Background

The following apply to me:


I program at work in Go
I program in Go outside of work
I program at work in another language
I manage a programming team
I am a student
Other


I write Go:


As part of my daily routine
Weekly
Monthly
Infrequently
Never


Where do you write Go code?


I program in Go at work and outside of work
I program in Go at work but not outside of work
I program in Go outside of work but not at work


I have used Go for:


Less than 3 months
3-12 months
13-24 months
2-4 years
4+ years


Rank the following languages in terms of your preference:


Go
Python
Rust
Javascript
Java
C#
Ruby
C
Typescript
C++
PHP
Kotlin
Haskell
Clojure
Swift
Scala
Bash
Elixir
Other


Rank the following languages in terms of your expertise:


Go
Python
Rust
Javascript
Java
C#
Ruby
C
Typescript
C++
PHP
Kotlin
Haskell
Clojure
Swift
Scala
Bash
Elixir
Other

Development domains

I work in the following areas (select all that apply):


Web development
DevOps
Systems programming
Network programming
Databases
Security
Data Science
Finance/Commerce
Desktop/GUI applications
Mobile
Embedded devices/Internet of Things
Machine Learning / Artificial Intelligence
Academic / Scientific / Numeric
Gaming
Other


I write the following in Go:


API/RPC services
A runnable/interactive program
Web services
Agents and daemons
Automation/scripts
Data processing
Libraries or Frameworks
Desktop/GUI applications
Games
Mobile Apps
Other
I don't write in Go


Which of the following best describes the industry in which your organization operates?


Intenet or web services
Software
Finance, banking or insurance
Media, advertising, publishing or entertainment
Education
Telecommunications
Retail or wholesale trades
Consulting
Other

Attitudes towards Go
Mention of NPS: https://en.wikipedia.org/wiki/Net_Promoter

This year we added a question asking "How likely are you to recommend Go to a friend or colleague?" to calculate our Net Promoter Score. This score attempts to measure how many more "promoters" a product has than "detractors" and ranges from -100 to 100; a positive value suggests most people are likely to recommend using a product, while negative values suggest most people are likely to recommend against using it. Our 2018 score is 61 (68% promoters - 7% detractors) and will serve as a baseline to help us gauge community sentiment towards the Go ecosystem over time.


To what extent do you agree or disagree with the following?


I would recommend Go to others
Overall, I am happy with Go
I would prefer to use Go for my next new project
Go is working well for my team
Go is critical to my company's success


If not for the following reasons, I would use Go more:


I work on an existing project written in another language
My project / team / TL prefers another language
Go lacks critical features
Go lacks critical libraries
Go isn't appropriate for what I'm working on
Go lacks critical tools
Not enough education or support resources
Go lacks critical performance
Other


What is the biggest challenge you personally face using Go today?


Modules/vendoring/package management
Differences from familiar languages/ecosystems
Lack of generics
GUI/front-end development
Error handling
Missing libraries
Debugging
Learning curve/understanding best practices
Code verbosity/code organization
Project structure/GOPATH
Business adoption
Missing language concepts
Performance problems


Importance vs. satisfaction of various factors


Binary size
Build speed
Memory usage
CPU usage
Debuggability


To what extend do you agree or disagree with the following statements


I have understanding of Go best practices
I am able to quickly find answers to my questions
Go's performance meets my needs
Go's support for language interoperability meets my needs
I am able to quickly find libraries that I need
The Go libraries I use have the stability and features I need
Go language, library, and tool documentation meet my needs
I am able to effectively diagnose bugs in my Go programs
I am able to effetively diagnose performance issues in Go programs
I am able to effectively use Go's concurrency features (goroutines, channels, select)
I am able to effectively debug uses of Go's concurrency features (goroutines, channels,...)

Development environments

I primarily develop Go on:


Only linux
linux and macos
only macos
linux and windows
only windows
linux and macos and windows
macos and windows


My preferred Go editor:


VS Code
GoLang/IntelliJ
Vim
Emacs
Sublime Text
Atom
Visual Studio
Other


What would most improve Go support in your preferred editor?


Better debugging support
Improved code completion
Better integration with CLI tools
Better module support
Performance/Speed
Improved error reporting
Find/implement interface
Nothing
Automated import management
Official language server


Rank the top three architectures for which Go support is most important for you:


Intel x86/x86-64
ARM64
WebAssembly
ARM
RISC-V
PPC64
MIPS/MIPS64
x390x

Deployments and Services

My team deploys Go programs to:


On-prem machines
AWS
GCP
Azure
Other clouds
None
At least one cloud
At least one cloud and on-prem
Multi-cloud
Containers
Serverless


Which of the following do you access from Go?


Open source relational DB
Cloud storage
Memory cache
Open source NoSQL DB
Authetication and federation
Distributed key-value store
Proprietary relational DB
Distributed lock service
Other

Go Community

Please rank the top 5 resources you use to get answesr to your Go related questions:


Stack Overflow
godoc.org
golang.org
Reading source code
GitHub
gobyexample.com
Books/ ebooks
Coworkers
Reddit
Youtube
Other


Please rank the top 5 resources you use to get Go news:


Reddit
Twitter
blog.golang.org
hacker news
golangweekly.com
coworkers
community blogs
github
just for func
youtube
other


I have or am interested in contributing in the following ways to the Go community and projects:


Standard libraries
Tools
Evangelism/ Advocacy
Documentation
Tutorials
Being a technical mentor
Community support via Stack Overflow, Slack, mailing lists, etc
Community involvement (workgroups, meetup attendance)
Toolchain (compiler, linker, etc)
Go project maintenance (issue triage)
Language translation
Event planning (meetup, conference, etc)
General UX & design contributions
golang.org website (code, UX, IA, content, etc.)
other


To what extend do youagree or disagree with the following statements?


I am confident in the leadership of Go
I feel welcome to contribute to Go
The process of contributing to Go is clear to me
I feel comfortable approaching the Go project leadership with questions and feedback
The Go project leadership understands my needs


I identify as


I do not identify as part of an underrepresented group
I prefer not to answer
I identify as ethnically or racially underrepresented
I identify as LGBTQIA
I identify as an underrepresented group not listed
I identify as neurodiverse or as having a disability
I identify as a woman
I identify as part of an underrepresented group, but prefer not to specify
no response


I feel welcome in the Go community


yes
no


What changes would make the Go community more welcome


Reduce perception of elitism
Increase transparency at leadership level
increase introductory resources
more events/meet-ups
other


```

----


# Appendix

## time periods
-[ ] Less than a day
-[ ] Less than a week
-[ ] Less than a month
-[ ] 1 to 3 months
-[ ] 3 to 6 months
-[ ] 6 to 12 months
-[ ] 1 to 2 years
-[ ] 2 to 3 years
-[ ] More than 3 years
